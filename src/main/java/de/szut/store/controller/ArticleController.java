package de.szut.store.controller;

import de.szut.store.dto.AddArticleDto;
import de.szut.store.dto.GetArticleDto;
import de.szut.store.dto.GetArticleInSpecificCurrencyDto;
import de.szut.store.dto.RateDto;
import de.szut.store.exceptionhandling.CurrencycodeNotFoundException;
import de.szut.store.model.Article;
import de.szut.store.service.ArticleService;
import de.szut.store.service.ExchangerateService;
import de.szut.store.service.MappingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/store/article")
public class ArticleController {
    private final ArticleService service;
    private final MappingService mappingService;
    private final ExchangerateService exchangerateService;

    public ArticleController(ArticleService service, MappingService mappingService, ExchangerateService exchangerateService) {
        this.service = service;
        this.mappingService = mappingService;
        this.exchangerateService = exchangerateService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<GetArticleDto>> getAllArticles(){
        List<Article> articles = this.service.readAll();
        List<GetArticleDto> list = new LinkedList<>();
        for (Article article: articles) {
            list.add(this.mappingService.mapArticleToGetArticleDto(article));
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetArticleDto> getArticleById(@PathVariable Long id) {
        Article article = this.service.readById(id);
        GetArticleDto dto = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<GetArticleDto> getArticleByDesignation(@RequestParam(name = "designation") final String designation){
        Article article = this.service.readByDesignation(designation);
        GetArticleDto dto = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/{articleId}/currency")
    public ResponseEntity<GetArticleInSpecificCurrencyDto> getArticleByIdAndCurrency(@RequestParam(name = "currency") final String currency, @PathVariable Long articleId){
        Article article = this.service.readById(articleId);
        RateDto actualPrice = this.exchangerateService.convert("EUR", currency, article.getPrice());
        if(actualPrice.getResult()==0.0){
            throw new CurrencycodeNotFoundException("Currencycode don't exist!");
        }
        GetArticleInSpecificCurrencyDto response = this.mappingService.mapArticleToGetArticleInSpecificCurrencyDto(article, actualPrice);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/{supplierId}")
    public ResponseEntity<GetArticleDto> createArticle(@PathVariable Long supplierId, @Valid @RequestBody final AddArticleDto dto){
        Article article= this.mappingService.mapAddArticleDtoToArticle(dto);
        article = this.service.createBySupplierId(supplierId, article);
        GetArticleDto request = this.mappingService.mapArticleToGetArticleDto(article);
        return new ResponseEntity<>(request, HttpStatus.CREATED);
    }
}