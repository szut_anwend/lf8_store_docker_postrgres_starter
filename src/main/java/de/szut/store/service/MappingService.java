package de.szut.store.service;

import de.szut.store.dto.*;
import de.szut.store.model.Article;
import de.szut.store.model.Contact;
import de.szut.store.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class MappingService {
    public Supplier mapAddSupplierDtoToSupplier(AddSupplierDto dto){
        Supplier newSupplier = new Supplier();
        newSupplier.setName(dto.getName());
        Contact newContact = new Contact();
        newContact.setStreet(dto.getStreet());
        newContact.setPostcode(dto.getPostcode());
        newContact.setCity(dto.getCity());
        newContact.setPhone(dto.getPhone());
        newSupplier.setContact(newContact);
        newContact.setSupplier(newSupplier);
        return newSupplier;
    }

    public GetSupplierDto mapSupplierToGetSupplierDto(Supplier supplier){
        GetSupplierDto dto = new GetSupplierDto();
        dto.setSid(supplier.getSid());
        dto.setName(supplier.getName());
        dto.setStreet(supplier.getContact().getStreet());
        dto.setPostcode(supplier.getContact().getPostcode());
        dto.setCity(supplier.getContact().getCity());
        dto.setPhone(supplier.getContact().getPhone());
        return dto;
    }

    public GetAllArticlesBySupplierIdDto mapSupplierToGetAllArticlesBySupplierIdDto(Supplier supplier){
        GetAllArticlesBySupplierIdDto dto = new GetAllArticlesBySupplierIdDto();
        dto.setSupplierId(supplier.getSid());
        dto.setName(supplier.getName());
        Set<GetArticleDto> allArticles = new HashSet<>();
        for (Article article: supplier.getArticles()) {
            GetArticleDto aDto = new GetArticleDto();
            aDto.setAid(article.getAid());
            aDto.setDesignation(article.getDesignation());
            aDto.setPrice(article.getPrice());
            allArticles.add(aDto);
        }
        dto.setAllArticles(allArticles);
        return dto;
    }

    public Article mapAddArticleDtoToArticle(AddArticleDto dto){
        Article entity = new Article();
        entity.setDesignation(dto.getDesignation());
        entity.setPrice(dto.getPrice());
        return entity;
    }

    public GetArticleDto mapArticleToGetArticleDto(Article entity){
        GetArticleDto dto = new GetArticleDto();
        dto.setAid(entity.getAid());
        dto.setDesignation(entity.getDesignation());
        dto.setPrice(entity.getPrice());
        return dto;
    }

    public GetArticleInSpecificCurrencyDto mapArticleToGetArticleInSpecificCurrencyDto(Article article, RateDto rateDto){
        GetArticleInSpecificCurrencyDto dto = new GetArticleInSpecificCurrencyDto();
        dto.setAid(article.getAid());
        dto.setDesignation(article.getDesignation());
        dto.setPrice(rateDto.getResult());
        dto.setCurrency(rateDto.getQuery().getTo());
        return dto;
    }
}
