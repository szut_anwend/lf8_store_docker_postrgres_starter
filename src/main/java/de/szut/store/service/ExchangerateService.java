package de.szut.store.service;

import de.szut.store.dto.RateDto;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ExchangerateService {

    private RestTemplate restTemplate;
    private final String url = "https://api.exchangerate.host/";

    public ExchangerateService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public RateDto convert(String from, String to, Double amount) {
        return this.restTemplate.getForObject(url + "convert?from={from}&to={to}&amount={amount}", RateDto.class, from, to, amount);
    }
}