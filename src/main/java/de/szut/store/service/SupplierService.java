package de.szut.store.service;

import de.szut.store.exceptionhandling.ResourceNotFoundException;
import de.szut.store.model.Article;
import de.szut.store.model.Supplier;
import de.szut.store.repository.SupplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class SupplierService {
    private final SupplierRepository repository;

    public SupplierService(SupplierRepository repository) {
        this.repository = repository;
    }

    public Supplier create(Supplier newSupplier) {
        return repository.save(newSupplier);
    }

    public List<Supplier> readAll() {
        List<Supplier> listSupplier = repository.findAll();
        return listSupplier;
    }

    public Supplier readById(long id) {
        Optional<Supplier> oSupplier = repository.findById(id);
        if (oSupplier.isEmpty()) {
            throw new ResourceNotFoundException("Supplier not found on id = " + id);
        }
        return oSupplier.get();
    }

    public Supplier update(Supplier supplier) {
        Supplier updatedSupplier = readById(supplier.getSid());
        updatedSupplier.setName(supplier.getName());
        updatedSupplier.getContact().setStreet(supplier.getContact().getStreet());
        updatedSupplier.getContact().setPostcode(supplier.getContact().getPostcode());
        updatedSupplier.getContact().setCity(supplier.getContact().getCity());
        updatedSupplier.getContact().setPhone(supplier.getContact().getPhone());
        updatedSupplier = this.repository.save(updatedSupplier);
        return updatedSupplier;
    }

    public void delete(long id) {
        Supplier toDelete = readById(id);
        repository.deleteById(id);
    }
}

