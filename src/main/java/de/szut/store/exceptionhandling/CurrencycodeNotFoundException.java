package de.szut.store.exceptionhandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class CurrencycodeNotFoundException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public CurrencycodeNotFoundException(String message){
        super(message);
    }
}