package de.szut.store.dto;

import lombok.Data;

@Data
public class GetArticleInSpecificCurrencyDto {
    private Long aid;
    private String designation;
    private Double price;
    private String currency;
}
