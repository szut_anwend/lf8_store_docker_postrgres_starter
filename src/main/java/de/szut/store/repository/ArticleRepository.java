package de.szut.store.repository;

import de.szut.store.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    public Article findByDesignation(String name);
}
