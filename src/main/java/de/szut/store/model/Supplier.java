package de.szut.store.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Entity
@Table(name="supplier")
public class Supplier {
    @Id
    @Column(name="sid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long sid;

    @Column(name="name")
    private String name;

    @OneToOne(fetch = FetchType.EAGER,
            cascade =  CascadeType.ALL)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    @OneToMany(mappedBy = "supplier", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Article> articles;

    public void addArticle(Article article){
        articles.add(article);
    }
}
