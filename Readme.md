# Requirements
* Docker https://docs.docker.com/get-docker/
* Docker compose (bei Windows und Mac schon in Docker enthalten) https://docs.docker.com/compose/install/

# Postgres
## Terminal öffnen
für alles gilt, im Terminal im Ordner docker/local sein
```bash
cd docker/local
```
## Postgres starten
```bash
docker compose up
```
Achtung: Der Docker-Container läuft dauerhaft! Wenn er nicht mehr benötigt wird, sollten Sie ihn stoppen.

## Postgres stoppen
```bash
docker compose down
```

## Postgres Datenbank wipen, z.B. bei Problemen
```bash
docker compose down
docker volume rm local_store_postgres_data
docker compose up
```

## initialer Zustand der Datenbank (flyway)
siehe src/main/resources/db/migrations/V1.0__init.sql. Hier können auch Ergänzungen vorgenommen werden, allerdings muss dann die Datenbank gewipt werden. Alterniv eine weitere Datei V1.1__Name.sql erstellen und dort Erweiterungen vornehmen.
